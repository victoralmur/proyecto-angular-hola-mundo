import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css'],
})
export class BodyComponent implements OnInit {

  mostrar = true;
  valorNumerico = 1;
  
  frase = {
    mensaje: 'La sabiduría viene de la experiencia. La experiencia es, a menudo, el resultado de la falta de sabiduría',
    autor: 'Terry Pratchett',
  };

  personas: string[] = ['Pedro', 'Carlos', 'Maria'];

  constructor() {}

  ngOnInit(): void {}
}
